
#include <stdio.h>
#include <stdlib.h>
#include <SDL/SDL.h>                                                                
#include <SDL/SDL_getenv.h>                                                         

/*
   apt-get update ; apt-get install libsdl1.2-dev gcc clang  

game:
	g++ `sdl-config --cflags --libs` game02.cpp -o /tmp/sdlanim  
	/tmp/sdlanim 
*/


#define SCREEN_WIDTH  1920
#define SCREEN_HEIGHT 1080
#define SPRITE_SIZE     32


int gameover;
int sprite4_direction = 1;

/* source and destination rectangles */
SDL_Rect rcSrc,  rcSprite;    // player 1
SDL_Rect rcSrc2, rcSprite2;   // player 2
SDL_Rect rcSrc3, rcSprite3;   // ghost 
SDL_Rect rcSrc4, rcSprite4;   // monster
SDL_Rect rcSrc5, rcSprite5;   // minotaur




void HandleEvent(SDL_Event event)
{
	switch (event.type) {
		/* close button clicked */
		case SDL_QUIT:
			gameover = 1;
			break;
			

		/* handle the keyboard */
		case SDL_KEYDOWN:
			switch (event.key.keysym.sym) {
				case SDLK_ESCAPE:
					gameover = 1;
					break;






                                // Player 2
				case SDLK_h:
					if ( rcSrc.x == 192 )
						rcSrc.x = 224;
					else
						rcSrc.x = 192;
					rcSprite.x -= 5;
					break;

				case SDLK_l:
					if ( rcSrc.x == 64 )
						rcSrc.x = 96;
					else
						rcSrc.x = 64;
					rcSprite.x += 5;
					break;

				case SDLK_k:
					if ( rcSrc.x == 0 )
						rcSrc.x = 32;
					else
						rcSrc.x = 0;
					rcSprite.y -= 5;
					break;

				case SDLK_j:
					if ( rcSrc.x == 128 )
						rcSrc.x = 160;
					else
						rcSrc.x = 128;
					rcSprite.y += 5;
					break;



                                // Player Minotaur
				case SDLK_y:
					if ( rcSrc5.x == 192 )
						rcSrc5.x = 224;
					else
						rcSrc5.x = 192;
					rcSprite5.x -= 5;
					break;

				case SDLK_v:
					if ( rcSrc5.x == 64 )
						rcSrc5.x = 96;
					else
						rcSrc5.x = 64;
					rcSprite5.x += 5;
					break;

				case SDLK_c:
					if ( rcSrc5.x == 0 )
						rcSrc5.x = 32;
					else
						rcSrc5.x = 0;
					rcSprite5.y -= 5;
					break;

				case SDLK_x:
					if ( rcSrc5.x == 128 )
						rcSrc5.x = 160;
					else
						rcSrc5.x = 128;
					rcSprite5.y += 5;
					break;












                                // Player 3
				case SDLK_a:
					if ( rcSrc3.x == 192 )
						rcSrc3.x = 224;
					else
						rcSrc3.x = 192;
					rcSprite3.x -= 5;
					break;

				case SDLK_d:
					if ( rcSrc3.x == 64 )
						rcSrc3.x = 96;
					else
						rcSrc3.x = 64;
					rcSprite3.x += 5;
					break;

				case SDLK_w:
					if ( rcSrc3.x == 0 )
						rcSrc3.x = 32;
					else
						rcSrc3.x = 0;
					rcSprite3.y -= 5;
					break;

				case SDLK_s:
					if ( rcSrc3.x == 128 )
						rcSrc3.x = 160;
					else
						rcSrc3.x = 128;
					rcSprite3.y += 5;
					break;




                                // Player 1
				case SDLK_LEFT:
					if ( rcSrc2.x == 192 )
						rcSrc2.x = 224;
					else
						rcSrc2.x = 192;
					rcSprite2.x -= 5;
					break;

				case SDLK_RIGHT:
					if ( rcSrc2.x == 64 )
						rcSrc2.x = 96;
					else
						rcSrc2.x = 64;
					rcSprite2.x += 5;
					break;

				case SDLK_UP:
					if ( rcSrc2.x == 0 )
						rcSrc2.x = 32;
					else
						rcSrc2.x = 0;
					rcSprite2.y -= 5;
					break;

				case SDLK_DOWN:
					if ( rcSrc2.x == 128 )
						rcSrc2.x = 160;
					else
						rcSrc2.x = 128;
					rcSprite2.y += 5;
					break;


			}
			break;
	}
}




int main(int argc, char* argv[])
{

	putenv((char*)"FRAMEBUFFER=/dev/fb0");                                      
	putenv((char*)"SDL_FBDEV=/dev/fb0");                                        

        // Define the primary surface for display                                           
	SDL_Surface *screen, *temp, *sprite, *grass, *ghost, *monster, *minotaur;
	SDL_Rect rcGrass;
	int colorkey;

	/* initialize SDL */
	SDL_Init(SDL_INIT_VIDEO);

	/* set the title bar */
	SDL_WM_SetCaption("SDL Animation", "SDL Animation");

	const SDL_VideoInfo* vInfo = SDL_GetVideoInfo();                            
	int     nResX = vInfo->current_w;                                           
	int     nResY = vInfo->current_h;                                           
	int     nDepth = vInfo->vfmt->BitsPerPixel;                                 

	printf(    "We are now on Framebuffer /dev/fb0 ... (use pi, /home/pi on retropie).\n" );
	printf(    "Resolution test: %d %d %d \n" ,  nResX , nResY , nDepth ); 

	int     nFlags = SDL_SWSURFACE;                                             
	screen = SDL_SetVideoMode(nResX,nResY,nDepth,nFlags);                      

	/* set keyboard repeat */
	SDL_EnableKeyRepeat(70, 70);

	/* load sprite */
	temp   = SDL_LoadBMP("sprite.bmp");
	sprite = SDL_DisplayFormat(temp);
	SDL_FreeSurface(temp);

	/* load ghost */
	temp   = SDL_LoadBMP("ghost.bmp");
	ghost = SDL_DisplayFormat(temp);
	SDL_FreeSurface(temp);

	/* load monster */
	temp   = SDL_LoadBMP("monster.bmp");
	monster = SDL_DisplayFormat(temp);
	SDL_FreeSurface(temp);

	/* load grass */
	temp  = SDL_LoadBMP("grass.bmp");
	grass = SDL_DisplayFormat(temp);
	SDL_FreeSurface(temp);

	/* load minotaur */
	temp   = SDL_LoadBMP("minotaur.bmp");
	minotaur = SDL_DisplayFormat(temp);
	SDL_FreeSurface(temp);

	/* setup sprite colorkey and turn on RLE */
	colorkey = SDL_MapRGB(screen->format, 255, 0, 255);
	SDL_SetColorKey(sprite, SDL_SRCCOLORKEY | SDL_RLEACCEL, colorkey);
	SDL_SetColorKey(ghost, SDL_SRCCOLORKEY | SDL_RLEACCEL, colorkey);
	SDL_SetColorKey(monster, SDL_SRCCOLORKEY | SDL_RLEACCEL, colorkey);
	SDL_SetColorKey(minotaur, SDL_SRCCOLORKEY | SDL_RLEACCEL, colorkey);





	/* set sprite position */
	rcSprite.x = 150;
	rcSprite.y = 150;

	rcSprite2.x = 100;
	rcSprite2.y = 100;

	rcSprite3.x = 200;
	rcSprite3.y = 150;

	rcSprite4.x = 50;
	rcSprite4.y = 50;

	rcSprite5.x = 250;
	rcSprite5.y = 250;


	/* set animation frame */
	rcSrc.x = 128;
	rcSrc.y = 0;
	rcSrc.w = SPRITE_SIZE;
	rcSrc.h = SPRITE_SIZE;

	rcSrc2.x = 128;
	rcSrc2.y = 0;
	rcSrc2.w = SPRITE_SIZE;
	rcSrc2.h = SPRITE_SIZE;

	rcSrc3.x = 128;
	rcSrc3.y = 0;
	rcSrc3.w = SPRITE_SIZE;
	rcSrc3.h = SPRITE_SIZE;

	rcSrc4.x = 128;
	rcSrc4.y = 0;
	rcSrc4.w = SPRITE_SIZE;
	rcSrc4.h = SPRITE_SIZE;

	rcSrc5.x = 128;
	rcSrc5.y = 0;
	rcSrc5.w = SPRITE_SIZE;
	rcSrc5.h = SPRITE_SIZE;


	gameover = 0;

        // direction of monster
	sprite4_direction  = 1;
	rcSrc4.x = 96;



	/* message pump */
	while (!gameover)
	{
		SDL_Event event;
		
		/* look for an event */
		if (SDL_PollEvent(&event)) {
			HandleEvent(event);
		}

		/* collide with edges of screen */
		if (rcSprite.x <= 0)
			rcSprite.x = 0;
		if (rcSprite.x >= SCREEN_WIDTH - SPRITE_SIZE) 
			rcSprite.x = SCREEN_WIDTH - SPRITE_SIZE;

		if (rcSprite.y <= 0)
			rcSprite.y = 0;
		if (rcSprite.y >= SCREEN_HEIGHT - SPRITE_SIZE) 
			rcSprite.y = SCREEN_HEIGHT - SPRITE_SIZE;

		/* draw the grass */
		for (int x = 0; x < SCREEN_WIDTH / SPRITE_SIZE; x++) {
			for (int y = 0; y < SCREEN_HEIGHT / SPRITE_SIZE; y++) {
				rcGrass.x = x * SPRITE_SIZE;
				rcGrass.y = y * SPRITE_SIZE;
				SDL_BlitSurface(grass, NULL, screen, &rcGrass);
			}
		}


                if ( rcSprite4.x >= 400 ) 
		{
		    sprite4_direction  = 2;
		    rcSrc4.x = 224;
		}
                else if ( rcSprite4.x <= 0 ) 
		{
		    sprite4_direction  = 1;
		    rcSrc4.x = 96;
		}

		if ( sprite4_direction == 1 ) 	 
		    rcSprite4.x += 5;
    	        else if ( sprite4_direction == 2) 	 
		    rcSprite4.x -= 5;


		/* draw the sprite */
		SDL_BlitSurface( sprite, &rcSrc, screen, &rcSprite);
		SDL_BlitSurface( sprite , &rcSrc2, screen ,  &rcSprite2);
		SDL_BlitSurface( ghost ,  &rcSrc3, screen ,  &rcSprite3);
		SDL_BlitSurface( monster ,   &rcSrc4, screen ,  &rcSprite4 );
		SDL_BlitSurface( minotaur ,  &rcSrc5, screen ,  &rcSprite5 );


		/* update the screen */
		SDL_UpdateRect(screen, 0, 0, 0, 0);
	}

	/* clean up */
	SDL_FreeSurface(sprite);
	SDL_FreeSurface(grass);
	SDL_Quit();

	return 0;
}




