
//////////////////////////////////////////
//////////////////////////////////////////
//////////////////////////////////////////
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <SDL/SDL.h>
/*
           apt-get update ; apt-get install libsdl1.2-dev gcc clang make 
	   gcc sdl.c -lSDL  -o /tmp/sdl-test 
*/


int main(int argc, char *argv[]) 
{
   SDL_Init(SDL_INIT_EVERYTHING);
   SDL_WM_SetCaption("SDL Example", NULL);
   int cols = 320;
   int rows = 240; 
   SDL_Surface *screen;  // create screen
   // screen = SDL_SetVideoMode(1024, 768, 0, SDL_SWSURFACE | SDL_FULLSCREEN);
   //screen = SDL_SetVideoMode( cols , rows, 32, SDL_HWSURFACE | SDL_FULLSCREEN ); 
   screen = SDL_SetVideoMode( cols , rows, 32, SDL_HWSURFACE  ); 
   SDL_putenv("SDL_VIDEO_CENTERED=center"); 
   SDL_Event event;

   int gameover = 0;
   while ( gameover == 0 )
   {
    // draw 
    SDL_FillRect( screen,  0 , SDL_MapRGB( screen->format, 0, 0, 0 ) );
    // rects
    //mvrectsdl( 100 , cols-200, 200, cols-10 , screen, 0x00FF00 );
    //mvrectsdl( 400 , cols-200, 500, cols-10 , screen, 0xFF0000 );
    // draw pixels to screen 
    SDL_Flip( screen ); //refresh
    // like getch
    SDL_WaitEvent(&event);

    if ( event.type == SDL_KEYDOWN )
      printf( "Keydown \n" );

    if ( event.type == SDL_KEYDOWN )
    switch( event.key.keysym.sym  )
    {
         case SDLK_ESCAPE:
           printf( "ESC\n" );
           gameover = 1;
           break;
         case SDLK_LEFT:
           printf( "LEFT\n" );
           break;
         case SDLK_RIGHT:
           printf( "RIGHT\n" );
           break;
         case SDLK_DOWN:
         case SDLK_j:
           printf( "DOWN\n" );
           break;
         case SDLK_UP:
         case SDLK_k:
           printf( "UP\n" );
           break;
         case SDLK_PAGEDOWN:
         case SDLK_d:
           printf( "PAGE DOWN\n" );
           break;
         case SDLK_PAGEUP:
         case SDLK_u:
           printf( "PAGE UP\n" );
           break;
         case SDLK_HOME:
         case SDLK_g:
           printf( "HOME\n" );
           break;
         case SDLK_RETURN:
           break;
    }
   }

   printf( "Bye...\n" );
   SDL_Quit();
   return 0;
}



